package com.oracle.candidate_appraisal_task.service;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class InterpreterServiceTest {

    @Test
    public void generatePath() {
        String path = InterpreterService.generatePath("python", "1568751797877");

        Assert.assertEquals("scripts/python/1568751797877.py", path);
    }

    @Test
    public void runSubProcess() {
    }

    @Test
    public void writeProgram() throws Exception {
        File tmpFile = new File("scripts/python/1568751797877.py");
        boolean exists = tmpFile.exists();
        InterpreterService.writeProgram("scripts/python/1568751797877.py", "print 'hello world'", "1568751797877");
        Assert.assertEquals(exists, exists);
    }

}