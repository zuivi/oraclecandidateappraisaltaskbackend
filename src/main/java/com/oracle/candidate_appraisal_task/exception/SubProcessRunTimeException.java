package com.oracle.candidate_appraisal_task.exception;

public class SubProcessRunTimeException extends RuntimeException {
    private String message = "Could not your program, verify syntax and launch again";

    public SubProcessRunTimeException() {
    }

    @Override
    public String getMessage() {
        return message;
    }
}
